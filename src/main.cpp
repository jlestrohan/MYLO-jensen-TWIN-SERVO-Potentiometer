// https://github.com/mensink/arduino-lib-MCP42010 <- library
// see https://www.bluedot.space/tutorials/how-many-devices-can-you-connect-on-i2c-bus/
// see http://www.henrykoch.de/en/arduino-control-and-test-digital-potentiometer-mcp42010-on-a-breadboard

// see https://stackoverflow.com/questions/44281734/mathematical-conversion-linear-input-curve-to-logarithm-like-output-curve-in-c
// log vs linear see https://www.maximintegrated.com/en/design/technical-documents/app-notes/3/3996.html
// https://books.google.fr/books?id=zd6cg63SLw0C&pg=PA508&lpg=PA508&dq=mcp42010+linear+logarithmic&source=bl&ots=8UR2vtcBd5&sig=ACfU3U0Cvbxq3ZmCVO_hBJstdtC1X7fBjg&hl=fr&sa=X&ved=2ahUKEwjq19vBwdDmAhUGa8AKHfySB6EQ6AEwCXoECAoQAQ

//

#include <math.h>
#include <tinySPI.h>
#include "Arduino.h"
#include "configuration.h"

#ifdef LCD_AVAILABLE
#include <LiquidCrystal_I2C.h>
#include <TinyWireM.h>
#endif

#ifdef LCD_AVAILABLE
LiquidCrystal_I2C lcd(LCD_ADDRESS, 16, 2); // set the LCD address to 0x27 for a 16 chars and 2 line display
#endif

// potentiometer value
uint8_t potValue = 0, oldPotValue = 0;
byte i = 0, length = 0;

#ifdef LCD_AVAILABLE
// ---------------------------------------------------------------------------------------------
void lcdTrailingSpaces(byte length)
{
    for (byte i = length + 5; i < 16; i++)
    {
        lcd.write(' ');
    }
}
#endif

/**
 * @brief  Change le contenu de *potVal en fonction d'un ratio pot/val prédéfini
 * @note   
 * @param  *potVal: 
 * @retval None
 */
void setLogPotValue(uint8_t *potVal)
{
    switch (*potVal)
    {
    case 0 ... 84:
        *potVal *= *potVal * 2 + 66;
        break;

    case 85 ... 94:
        *potVal *= 234;
        break;

    case 95 ... 104:
        *potVal *= 236;
        break;

    case 105 ... 114:
        *potVal *= 238;
        break;

    case 115 ... 124:
        *potVal *= 240;
        break;

    case 125 ... 134:
        *potVal *= 242;
        break;

    case 135 ... 144:
        *potVal *= 244;
        break;

    case 145 ... 154:
        *potVal *= 245;
        break;

    case 155 ... 164:
        *potVal *= 246;
        break;

    case 165 ... 174:
        *potVal *= 247;
        break;

    case 175 ... 184:
        *potVal *= 248;
        break;

    case 185 ... 194:
        *potVal *= 249;
        break;

    case 195 ... 204:
        *potVal *= 250;
        break;

    case 205 ... 214:
        *potVal *= 251;
        break;

    case 215 ... 224:
        *potVal *= 252;
        break;

    case 225 ... 234:
        *potVal *= 253;
        break;

    case 235 ... 244:
        *potVal *= 254;
        break;

    case 245 ... 255:
        *potVal *= 255;
        break;

    default:
        *potVal *= 0;
        break;
    }
}

/**
 * @brief  Writes a value to the digital pot
 * @note   
 * @param  address: 
 * @param  value: 
 * @retval None
 */
void MCP42010PotWrite(int address, uint8_t *value)
{
    // take the SS pin low to select the chip:
    digitalWrite(SPI_LATCH, LOW);
    //  send in the address and value via SPI:
    SPI.transfer(address);
    SPI.transfer(*value);
    // take the SS pin high to de-select the chip:
    digitalWrite(SPI_LATCH, HIGH);
}

/**
 * @brief  Setup function
 * @note   
 * @retval None
 */
void setup()
{
#ifdef TESTLED_AVAILABLE
    pinMode(TESTLED_PIN, OUTPUT); // initialize digital pin LED_BUILTIN as an output.
#endif
    pinMode(INPUT_POT_PIN, INPUT);
    pinMode(SPI_LATCH, OUTPUT); // set up the hardware SS pin (the SPI library sets up the clock and data pins)

    potValue = map(analogRead(INPUT_POT_PIN), 0, 1023, 0, 255);
    oldPotValue = potValue;

#ifdef LCD_AVAILABLE
    TinyWireM.begin();
    TinyWireM.beginTransmission(LCD_ADDRESS);

    lcd.init();
    lcd.backlight();
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Pot: 0");
    lcd.setCursor(0, 1);
    lcd.print("Res: 10k");

    lcd.setCursor(5, 0);
    byte length = lcd.print(potValue);
    for (byte i = length + 5; i < 16; i++)
    {
        lcd.write(' ');
    }

    TinyWireM.endTransmission();
#endif

    SPI.begin(); // start hardware SPI
    setLogPotValue(&potValue);
    MCP42010PotWrite(B00010001, &potValue); //	1st wiper
    MCP42010PotWrite(B00010010, &potValue); //  2nd wiper
    SPI.end();
}

/**
 * @brief  Main function
 * @note   
 * @retval 
 */
void loop()
{

    potValue = map(analogRead(INPUT_POT_PIN), 0, 1023, 0, 255);

    if ((potValue + 2) <= oldPotValue || (potValue - 2) >= oldPotValue)
    {
        oldPotValue = potValue;
        setLogPotValue(&potValue);
        // ****************************************** SPI transmission ************************************
        SPI.begin();
        MCP42010PotWrite(B00010001, &potValue); //	1st wiper
        MCP42010PotWrite(B00010010, &potValue); //  2nd wiper
        digitalWrite(TESTLED_PIN, HIGH);
        SPI.end();
        // ****************************************** SPI transmission end ********************************
        delay(30);

#ifdef LCD_AVAILABLE
        // ****************************************** I2C transmission ************************************
        TinyWireM.beginTransmission(LCD_ADDRESS);
        lcd.setCursor(5, 0);
        byte length = lcd.print(potValue);
        lcdTrailingSpaces(length);

        lcd.setCursor(5, 1);
        length = lcd.print(10000 * potValue / 256 + 52 + "R"); // displays resistance
        lcdTrailingSpaces(length);

        TinyWireM.endTransmission();
        // ****************************************** I2C transmission end ********************************
#endif
#ifdef TESTLED_AVAILABLE
        digitalWrite(TESTLED_PIN, LOW);
#endif
        delay(30);
    }
}
