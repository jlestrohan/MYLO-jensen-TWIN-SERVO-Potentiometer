#ifndef __CONFIGURATION_H     
#define __CONFIGURATION_H


// pin definitions
// SPI pins for AT44 are CS= 10, CLK = 4, DO= 5
#define SPI_LATCH 10  // storage register clock (slave select) -- hardware
#define TESTLED_PIN 9
#define INPUT_POT_PIN 2

#define TESTLED_AVAILABLE
//#define LCD_AVAILABLE
#define LCD_ADDRESS         0x27

#endif